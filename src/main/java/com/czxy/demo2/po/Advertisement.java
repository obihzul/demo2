package com.czxy.demo2.po;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import javax.persistence.*;

public class Advertisement {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 广告标题
     */
    private String title;

    /**
     * 广告内容
     */
    private String content;

    /**
     * 广告创建时间
     */
    @Column(name = "creation_time")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date creationTime;

    /**
     * 获取主键
     *
     * @return id - 主键
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取广告标题
     *
     * @return title - 广告标题
     */
    public String getTitle() {
        return title;
    }

    /**
     * 设置广告标题
     *
     * @param title 广告标题
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 获取广告内容
     *
     * @return content - 广告内容
     */
    public String getContent() {
        return content;
    }

    /**
     * 设置广告内容
     *
     * @param content 广告内容
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * 获取广告创建时间
     *
     * @return creation_time - 广告创建时间
     */
    public Date getCreationTime() {
        return creationTime;
    }

    /**
     * 设置广告创建时间
     *
     * @param creationTime 广告创建时间
     */
    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }
}