package com.czxy.demo2.service;

import com.czxy.demo2.po.Advertisement;

import java.util.List;

public interface AdvertisementService {
    Advertisement getAdvert();

    List<Advertisement> getByPage();
}
