package com.czxy.demo2.service.impl;

import com.czxy.demo2.mapper.AdvertisementMapper;
import com.czxy.demo2.po.Advertisement;
import com.czxy.demo2.service.AdvertisementService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class AdvertisementServiceImpl implements AdvertisementService {

    @Resource
    private AdvertisementMapper advertisementMapper;

    @Override
    public Advertisement getAdvert() {
        return advertisementMapper.getAdvert();
    }

    @Override
    public List<Advertisement> getByPage() {
        return advertisementMapper.selectAll();
    }
}
