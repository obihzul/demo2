package com.czxy.demo2.mapper;

import com.czxy.demo2.po.Advertisement;
import com.czxy.demo2.utils.MapperUtils;
import org.apache.ibatis.annotations.Select;

public interface AdvertisementMapper extends MapperUtils<Advertisement> {

    @Select("SELECT * FROM `advertisement` ORDER BY creation_time DESC LIMIT 0,1")
    Advertisement getAdvert();

}