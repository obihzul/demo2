package com.czxy.demo2.controller;

import com.czxy.demo2.po.Advertisement;
import com.czxy.demo2.service.AdvertisementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author: 武钊 ( oahzuw@gmail.com )
 * @Date: 2018/8/6 11:42
 * @Description:
 */
@Controller
public class HomeController {

    @Resource
    private AdvertisementService advertisementService;

    @RequestMapping("/index")
    public String index() {
        return "index";
    }

    @RequestMapping("top")
    public String top() {
        return "top";
    }

    @RequestMapping("list")
    public String list() {
        return "list";
    }

    @RequestMapping("/getAdvert")
    @ResponseBody
    public Advertisement getAdvert(){
        return advertisementService.getAdvert();
    }

    @RequestMapping("/getByPage")
    @ResponseBody
    public List<Advertisement> getByPage(){
        return advertisementService.getByPage();
    }

}
